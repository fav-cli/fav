package main

import (
	"os"

	"gitlab.com/fav-cli/fav/cmd/fav"
)

func main() {
	fav := fav.NewCommand()
	if err := fav.Execute(); err != nil {
		os.Exit(1)
	}
}
