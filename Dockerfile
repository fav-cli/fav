FROM alpine:3.19

WORKDIR /app

COPY ./bin/fav /usr/sbin/fav

CMD ["fav"]
