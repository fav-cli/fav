# Changelog

## [0.1.1] - 2024-03-02

### Bug Fixes

- Filepath separator lookup error when downloading secret

### Miscellaneous Tasks

- Ignore dependencies directories
- Add go releaser config and more complete makefile

## [0.1.0] - 2024-03-01

### Bug Fixes

- Close file before remove in decrypt
- Handle file closing error
- Remove backup option
- Change default value for remove flag
- Sanitize filepath on windows before uploading
- Sanitize filepath if path specified in config file

### Features

- Support versioning with --version

### Miscellaneous Tasks

- Support version from tag
- Compile and build docker only if tags is pushed
- Add release stage using goreleaser

### Release

- V0.1.0

## [0.0.6] - 2024-02-29

### Bug Fixes

- Close file before removing file

### Release

- V0.0.6

## [0.0.5] - 2024-01-27

### Bug Fixes

- Change options type so it doesn't bump with completion cmd
- Missing backup param when using encrypt in example decrypt

### Documentation

- Godoc comment for every function
- Update docs after adding backup option
- Change comment from block comment to single comment
- Provides simple docs for package configuration
- Provide docs for package in cmd
- Update readme
- Add logo to readme
- Resize logo in readme

### Features

- Support command completion
- Add backup option
- Log backup file operation
- Give shorthand option for backup

### Miscellaneous Tasks

- Add contributing and code of conduct
- Add program name in license
- Add logo image

### Release

- V0.0.5

## [0.0.4] - 2024-01-21

### Documentation

- Update readme
- Update changelog

### Features

- Move project to fav-cli group
- Add remove options
- Store encrypted file in buffer instead of file
- Add encrypt and decrypt example
- Add license

### Miscellaneous Tasks

- Create dockerfile
- Change base image to alpine

## [0.0.3] - 2024-01-12

### Bug Fixes

- Change cobra command logic

### Documentation

- Update readme
- Add newline between release changelog
- Update changelog

### Features

- Move to gitlab
- Store downloaded secret in buffer instead of .tmp folder

### Miscellaneous Tasks

- Add pipeline

## [0.0.2] - 2024-01-02

### Bug Fixes

- Reformat go.mod
- Change argument number condition
- Reformat encrypt decrypt flag reading
- Remove unnecessary jobs

### Documentation

- Update CHANGELOG.md for v0.0.2 release

### Features

- Store flags in viper instead of global variables
- Bind root command to function
- Add secret-key and secret-file flags
- Make config file type dynamic (json, yaml, toml)
- Bind flag to env
- Implement age encryption
- Update go.mod and go.sum
- Add github actions workflow for changelog and binary
- Add util function for getting file size and mode
- Support decryption
- Handle encrypt and decrypt flag
- Add config struct
- Combine ageEncrypt with encrypt and ageDecrypt with decrypt
- Implement storage upload and download
- Implement gcs upload and download
- Implement s3 upload and download
- Add more utility for file handling and naming
- Update go.mod and go.sum

### Refactor

- Improve age encryption logic

## [0.0.1] - 2023-12-09

### Bug Fixes

- Change config declaration syntax
- Remove .yaml on config error log

### Features

- Add base cli
- Fork hashicorp shamir lib
- Add minimum number of args
- Change encrypt and decrypt command to options
- Add changelog


