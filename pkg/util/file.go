package util

import (
	"crypto/md5"
	"fmt"
	"io/fs"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/fav-cli/fav/pkg/logger"
	"k8s.io/apimachinery/pkg/util/rand"
)

var log = logger.GetLogger()

// GetFileSize returns the size of the specified file.
func GetFileSize(file *os.File) int {
	fileStat, err := file.Stat()
	if err != nil {
		log.Error(err)
	}

	return int(fileStat.Size())
}

// GetFileMode returns the mode of the specified file.
func GetFileMode(file *os.File) fs.FileMode {
	fileStat, err := file.Stat()
	if err != nil {
		log.Error(err)
	}

	return fileStat.Mode()
}

// GetEncFileName generates the encrypted file name with the ".age" extension.
func GetEncFileName(file *os.File) string {
	name := fmt.Sprintf("%s.age", file.Name())

	return name
}

// GetDecFileName generates the decrypted file name by removing the ".age"
// extension.
func GetDecFileName(file *os.File) string {
	name := strings.TrimSuffix(file.Name(), ".age")

	return name
}

// GenerateSecretFileName generates a unique secret file name based on the MD5
// hash and a random string.
func GenerateSecretFileName(name string) string {
	f, err := os.ReadFile(name)
	if err != nil {
		log.Error(err)
	}

	md5Name := md5.Sum(f)
	randName := rand.String(len(name))

	secretFileName := fmt.Sprintf("%x.%s", md5Name, randName)

	return secretFileName
}

// GetMd5Sum calculates the MD5 hash sum of the specified file.
func GetMd5Sum(name string) string {
	f, err := os.ReadFile(name)
	if err != nil {
		log.Error(err)
	}

	md5Name := md5.Sum(f)

	return fmt.Sprintf("%x", md5Name)
}

// GetFilesInDirectory returns a slice containing the paths of all files in the
// specified directory.
func GetFilesInDirectory(dir string) ([]string, error) {
	var files []string

	err := filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		if !info.IsDir() {
			files = append(files, path)
		}

		return nil
	})

	if err != nil {
		return nil, err
	}

	return files, nil
}
