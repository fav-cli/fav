package config

import "gitlab.com/fav-cli/fav/pkg/storage"

type Config struct {
	Storages []storage.Storage `mapstructure:"storages"`
}
