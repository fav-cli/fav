package crypto

import (
	"bytes"
	"io"
	"os"
	"strings"

	"filippo.io/age"
	"filippo.io/age/armor"
	"github.com/spf13/viper"
	"gitlab.com/fav-cli/fav/pkg/config"
	"gitlab.com/fav-cli/fav/pkg/util"
	"gitlab.com/fav-cli/fav/shamir"
)

// Decrypt decrypts a file using age identity obtained through shamir secret
// sharing.
func Decrypt(name string, remove bool) error {
	file, err := os.Open(name)
	if err != nil {
		return err
	}
	defer file.Close()

	var config config.Config
	if err := viper.Unmarshal(&config); err != nil {
		return err
	}

	md5Sum := util.GetMd5Sum(name)

	var sharedSecret [][]byte
	for _, storage := range config.Storages {
		secret, err := storage.Download(md5Sum, remove)
		if err != nil {
			return err
		}

		sharedSecret = append(sharedSecret, secret)
	}

	identity, err := shamirCombine(sharedSecret)
	if err != nil {
		return err
	}

	if err := ageDecrypt(identity, file); err != nil {
		return err
	}

	return nil
}

// ageDecrypt decrypts a file using the provided age identity.
func ageDecrypt(identity *age.X25519Identity, file *os.File) error {
	content := make([]byte, util.GetFileSize(file))
	if _, err := file.Read(content); err != nil {
		return err
	}

	out := &bytes.Buffer{}
	f := strings.NewReader(string(content))
	armorReader := armor.NewReader(f)

	r, err := age.Decrypt(armorReader, identity)
	if err != nil {
		return err
	}

	if _, err := io.Copy(out, r); err != nil {
		return err
	}

	if err := os.WriteFile(util.GetDecFileName(file), out.Bytes(), util.GetFileMode(file)); err != nil {
		return err
	}

	if err := file.Close(); err != nil {
		return err
	}

	if err := os.Remove(file.Name()); err != nil {
		return err
	}

	return nil
}

// shamirCombine combines shared secrets into an age identity.
func shamirCombine(sharedSecret [][]byte) (*age.X25519Identity, error) {
	identityBytes, err := shamir.Combine(sharedSecret)
	if err != nil {
		return nil, err
	}

	identity, err := age.ParseX25519Identity(string(identityBytes))
	if err != nil {
		return nil, err
	}

	return identity, nil
}
