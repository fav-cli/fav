package crypto_test

import (
	"log"
	"os"

	"gitlab.com/fav-cli/fav/pkg/crypto"
)

func ExampleDecrypt() {
	tmpFile, err := os.CreateTemp("/tmp", "fav-test-file")
	if err != nil {
		log.Fatal(err)
	}
	defer tmpFile.Close()

	_, err = tmpFile.WriteString("test data")
	if err != nil {
		log.Fatal(err)
	}

	err = crypto.Encrypt(tmpFile.Name())
	if err != nil {
		log.Fatal(err)
	}

	err = crypto.Decrypt(tmpFile.Name(), false)
	if err != nil {
		log.Fatal(err)
	}
}
