package crypto

import (
	"io"
	"os"

	"filippo.io/age"
	"filippo.io/age/armor"
	"github.com/spf13/viper"
	"gitlab.com/fav-cli/fav/pkg/config"
	"gitlab.com/fav-cli/fav/pkg/logger"
	"gitlab.com/fav-cli/fav/pkg/util"
	"gitlab.com/fav-cli/fav/shamir"
)

// SharedSecrets represents a collection of shared secrets.
type SharedSecrets struct {
	Data []SharedSecret
}

// SharedSecret contains information about a shared secret.
type SharedSecret struct {
	FileName string
	Data     []byte
}

// Encrypt encrypts the specified file using age encryption and distributes the
// resulting identity using Shamir's Secret Sharing. 
func Encrypt(name string) error {
	file, err := os.Open(name)
	if err != nil {
		return err
	}
	defer file.Close()

	identity, err := ageEncrypt(file)
	if err != nil {
		return err
	}

	var config config.Config
	if err := viper.Unmarshal(&config); err != nil {
		return err
	}

	sharedSecret, err := shamirSplit(identity, util.GetEncFileName(file), len(config.Storages))
	if err != nil {
		return err
	}

	for i, storage := range config.Storages {
		fileName := sharedSecret.Data[i].FileName
		data := sharedSecret.Data[i].Data
		if err := storage.Upload(fileName, data); err != nil {
			return err
		}
	}

	return err
}

// ageEncrypt generates an age identity and encrypts a file using that
// identity. It returns the generated identity.
func ageEncrypt(file *os.File) (*age.X25519Identity, error) {
	log := logger.GetLogger()

	identity, err := age.GenerateX25519Identity()
	if err != nil {
		return nil, err
	}

	recipient := identity.Recipient()

	log.Infof(
		"running: age --encrypt --armor --recipient %s > %s && rm %s",
		recipient,
		util.GetEncFileName(file),
		file.Name(),
	)

	encFile, err := os.Create(util.GetEncFileName(file))
	if err != nil {
		return nil, err
	}

	armorWriter := armor.NewWriter(encFile)

	w, err := age.Encrypt(armorWriter, recipient)
	if err != nil {
		return nil, err
	}

	if _, err := io.Copy(w, file); err != nil {
		return nil, err
	}

	if err := w.Close(); err != nil {
		return nil, err
	}

	if err := armorWriter.Close(); err != nil {
		return nil, err
	}

	if err := file.Close(); err != nil {
		return nil, err
	}

	if err := os.Remove(file.Name()); err != nil {
		return nil, err
	}

	return identity, nil
}

// shamirSplit is a wrapper for splitting an age identity into files using the
// original file name as a suffix and returns an array of file names.
func shamirSplit(identity *age.X25519Identity, fileName string, parts int) (*SharedSecrets, error) {
	shares, err := shamir.Split([]byte(identity.String()), parts, parts)
	if err != nil {
		return nil, err
	}

	var sharedSecret SharedSecrets

	for _, s := range shares {
		var splitSecret SharedSecret

		splitSecret.FileName = util.GenerateSecretFileName(fileName)
		splitSecret.Data = s

		sharedSecret.Data = append(sharedSecret.Data, splitSecret)
	}

	return &sharedSecret, nil
}
