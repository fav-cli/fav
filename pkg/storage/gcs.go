package storage

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"path/filepath"
	"time"

	"cloud.google.com/go/storage"
	"google.golang.org/api/iterator"
)

// GcsBucket represents a Google Cloud Storage bucket configuration.
type GcsBucket struct {
	Bucket string `mapstructure:"bucket"`
	Path   string `mapstructure:"path,omitempty"`
}

// Upload uploads data to the specified object in the Google Cloud Storage
// bucket.
func (gcs *GcsBucket) Upload(object string, data []byte) error {
	ctx := context.Background()

	client, err := storage.NewClient(ctx)
	if err != nil {
		return fmt.Errorf("storage.NewClient: %s", err)
	}
	defer client.Close()

	ctx, cancel := context.WithTimeout(ctx, time.Second*50)
	defer cancel()

	if len(gcs.Path) != 0 {
		object = filepath.ToSlash(filepath.Join(gcs.Path, object))
	}

	o := client.Bucket(gcs.Bucket).Object(object)

	o = o.If(storage.Conditions{DoesNotExist: true})

	wc := o.NewWriter(ctx)
	rc := bytes.NewReader(data)
	if _, err = io.Copy(wc, rc); err != nil {
		return err
	}

	if err := wc.Close(); err != nil {
		return err
	}

	return nil
}

// Download downloads data from the Google Cloud Storage bucket with the
// specified prefix. If remove is true, it deletes the object after download.
func (gcs *GcsBucket) Download(prefix string, remove bool) ([]byte, error) {
	var object string

	ctx := context.Background()
	client, err := storage.NewClient(ctx)
	if err != nil {
		return nil, fmt.Errorf("storage.NewClient: %w", err)
	}
	defer client.Close()

	ctx, cancel := context.WithTimeout(ctx, time.Second*10)
	defer cancel()

	if len(gcs.Path) != 0 {
		prefix = filepath.ToSlash(filepath.Join(gcs.Path, object))
	}

	it := client.Bucket(gcs.Bucket).Objects(ctx, &storage.Query{
		Prefix:    prefix,
		Delimiter: "_",
	})

	for {
		attrs, err := it.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			return nil, fmt.Errorf("Bucket(%q).Objects(): %w", gcs.Bucket, err)
		}

		object = attrs.Name
	}

	var secret bytes.Buffer

	rc, err := client.Bucket(gcs.Bucket).Object(object).NewReader(ctx)
	if err != nil {
		return nil, fmt.Errorf("Object(%q).NewReader: %w", object, err)
	}
	defer rc.Close()

	if _, err := io.Copy(&secret, rc); err != nil {
		return nil, fmt.Errorf("io.Copy: %w", err)
	}

	if remove {
		o := client.Bucket(gcs.Bucket).Object(object)

		attrs, err := o.Attrs(ctx)
		if err != nil {
			return nil, fmt.Errorf("object.Attrs: %w", err)
		}
		o = o.If(storage.Conditions{GenerationMatch: attrs.Generation})

		if err := o.Delete(ctx); err != nil {
			return nil, fmt.Errorf("Object(%q).Delete: %w", object, err)
		}
	}

	return secret.Bytes(), nil
}
